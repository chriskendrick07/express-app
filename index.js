var express = require('express')
var app = express()
var bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var items = [];


app.get('/list', (req, res) => {
	res.send(items);
});

app.post('/list', (req, res, next) => {
	items.push(req.body.name);
	res.send(items);
});


app.get('/list/:id', (req, res) => {
	var id = req.params.id;
	if(items[id] == null || items[id] == undefined) res.status(400).send(`Item with index ${id} not found`);
	else res.send(items[id]);
});

app.put('/list/:id', (req, res) => {
	var id = req.params.id;
	if(items[id] == null || items[id] == undefined) res.status(400).send(`Item with index ${id} not found`);
	else items[id] = req.body.name; res.send(items[id]);
});

app.delete('/list/:id', (req, res, next) => {
	var id = req.params.id;
	console.log("scsvsdv", req.params)
	if(items[id] == null || items[id] == undefined) return res.status(400).send(`Item with index ${id} not found`);
	else items.splice(id, 1); res.send(items);
});

app.listen(3000, () => { console.log('Listening on port 3000')  })